<?php
defined('DEBUG_SEPARATOR') or define('DEBUG_SEPARATOR', "debug ================================================================");
// include('../dhelp/common.php');
include_once('autoload.php');

if (!function_exists('ds')) {
    function ds($n = 72)
    {
        echo str_repeat("=", $n);
    }
}

if (!function_exists('preout')) {
    function preout($v)
    {
        echo "<pre>";
        var_dump($v);
        echo "</pre>";
    }
}

if (!function_exists('preson')) {
    function preson($v)
    {
        echo "<pre>";
        echo json_encode($v,JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        echo "</pre>";
    }
}

if (!function_exists('presonRet')) {
    function presonRet($v)
    {
        return json_encode($v,JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }
}

if (!function_exists('jsonResponse')) {
    function jsonResponse($respCode,$msg,$data)
    {
        $r=[
            'status' => $respCode ,
            'message' => $msg ,
            'data' => $data
        ];
        return response()->json($r, $respCode);
    }
}

if (!function_exists('toFile')) {
    function toFile($content = "cnt", $file = "file")
    {
        $f = fopen($file, "w");
        fwrite($f,$content);
        fclose($f);
        return true;
    }
}

if (!function_exists('arrayGet')) {
    function arrayGet($array, $key, $default = NULL)
    {
        return isset($array[$key]) ? $array[$key] : $default;
    }
}

//get stisla file from 
if (!function_exists('stisla')) {
    function stisla($file)
    {
        return config('view.ui.template').$file;
    }
}

// src="{{ architect('$1')}}"></script>
if (!function_exists('architect')) {
    function architect($file)
    {
        return config('view.ui.architect.template').$file;
    }
}

if (!function_exists('getSessKeyFromState')) {
    // $referer = $_SERVER['HTTP_REFERER'];
    // get sesskey
    function getSessKeyFromState($referer = null)
    {
        // preout($_SESSION);
        // preout($_SESSION['USER']->sesskey);
        return $_SESSION['USER']->sesskey;
        // preout($_SERVER);
        // preout($referer);

        $kejaksaan = strpos($referer, "https://api.kejaksaan.go.id") === 0;
        // preson($kejaksaan);
        // exit;
        if(!$kejaksaan){
            return false;
        }
        
        $sesskey = "-";
        $parsedReferer = parse_url($referer);
        
        parse_str($parsedReferer['query'], $queries);
        parse_str($queries['state'], $queries2);
    
        $q1sesskey = array_key_exists('sesskey', $queries2);
        $q2sesskey = array_key_exists('amp;sesskey', $queries2);
        
        if($q1sesskey){
            // preout($queries2['amp;sesskey']);
            $sesskey = $queries2['sesskey'];
            return $sesskey;
        }
        
        if($q2sesskey){
            // preout($queries2['amp;sesskey']);
            $sesskey = $queries2['amp;sesskey'];
            return $sesskey;
        }
        
        // preout(array_key_exists('sesskey', $queries2));

        return false;
    }
}

if (!function_exists('decodeJWT')) {
    function decodeJWT($jwt) {
        $exp = explode(".", $jwt);
        $decoded = [];
        
        foreach($exp as $e){
            $decoded[] = base64_decode($e);
        }
        
        return $decoded;
    }
    
}

if (!function_exists('anc')) {
    // clean sesskey
    function anc($link)
    {
        ?>
        teruskan : 
        <a href="<?= $link ?>"><?= $link ?></a>
        <?php
    }
}

if (!function_exists('zipDL')) {
    //ty https://stackoverflow.com/questions/20162360/zip-and-download-files-using-php
    // dhelp/dl.php
    function zipDL($files = [], $filename = "download.zip")
    {
		# create new zip opbject
		$zip = new ZipArchive();

		# create a temp file & open it
		$tmp_file = tempnam('.','');
		$zip->open($tmp_file, ZipArchive::CREATE);
		
		//append filelist
		$f1 = fopen('list','w');
		fwrite($f1, presonRet($files));
		fclose($f1);
		
		$files[] = "list";

		# loop through each file
		foreach($files as $file){

			# download file
			$download_file = file_get_contents($file);

			#add it to the zip
			$zip->addFromString(basename($file),$download_file);

		}

		# close zip
		$zip->close();

		# send the file to the browser as a download
		header('Content-disposition: attachment; filename='.$filename);
		header('Content-type: application/zip');
		readfile($tmp_file);
    }
}
