<?php
defined('DEBUG_SEPARATOR') or define('DEBUG_SEPARATOR', "debug ================================================================");


$includes = [
    "Kejaksaan/OauthJaksa.php",
    "Kejaksaan/BaseAccessToken.php",
    "Kejaksaan/SimpleHTTPClient.php",
    "Kejaksaan/EmonevClient.php",
];
foreach($includes as $i){
    include_once($i);
}