<?php
include_once(__DIR__.'/../../dhelp/common.php');
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Open ID authentication. This file is a simple login entry point for OAuth identity providers.
 *
 * @package auth_oauth2
 * @copyright 2017 Damyon Wiese
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 */

require_once('../../config.php');

// $issuerid = 1;
$issuerid = required_param('id', PARAM_INT);
$wantsurl = new moodle_url(optional_param('wantsurl', '', PARAM_URL));

require_sesskey();

if (!\auth_oauth2\api::is_enabled()) {
    throw new \moodle_exception('notenabled', 'auth_oauth2');
}

$issuer = new \core\oauth2\issuer($issuerid);

$returnparams = ['wantsurl' => $wantsurl, 'sesskey' => sesskey(), 'id' => $issuerid];
$returnurl = new moodle_url('/auth/oauth2/login.php', $returnparams);

$client = \core\oauth2\api::get_user_oauth_client($issuer, $returnurl);


if ($client) {
    if (!$client->is_logged_in()) {
        $link = $client->get_login_url();
        redirect($client->get_login_url());
    }
    
    // debug scope ================================================================
    
    // save token to db via emonev
    $emc = new EmonevClient();
    
    $callback = function() use ($emc, $client, $issuer){
        try{
            $accToken = $client->get_accesstoken();
            $refToken = $client->getRefreshToken();
            
            $decoded = decodeJWT($accToken->token);
            $jwtPayloadUser = json_decode($decoded[1]);
            $pubUserInfo = $client->getPubUserinfo();
            /*
            1. new or update user as oauth pending
            2. prepare data
            3. call emonev w/ prepared data to save token
            */
            $_SESSION['access_token'] = $accToken;
            $_SESSION['refresh_token'] = $refToken;
            
            $batObjStr = (new BaseAccessToken)->encode(
                    $issuer->get('id'),
                    $jwtPayloadUser->sub,
                    null,
                    $accToken->token,
                    $refToken,
                    date("Y-m-d H:i:s", $jwtPayloadUser->exp),
                    $jwtPayloadUser->role,
                    $jwtPayloadUser->uid,
                    json_encode($pubUserInfo)
                );
            $emc->submitToken($batObjStr);
            
        }catch(Exception $e){
            echo $e->getMessage();
        }
    };
    // ./debug scope ================================================================
    // exit;
    // skrg
    $auth = new \auth_oauth2\auth();
    $auth->complete_login($client, $wantsurl, $callback);
} else {
    throw new moodle_exception('Could not get an OAuth client.');
}

