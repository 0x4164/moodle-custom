<?php
defined('DEBUG_SEPARATOR') or define('DEBUG_SEPARATOR', "debug ================================================================");

/*
c_ = custom

CREATE TABLE `c_access_token` (
  `id` bigint(10) NOT NULL PRIMARY KEY AUTO_INCREMENT ,
  `issuerid` bigint(10) NULL,
  `sub` varchar(255),
  `iduser` bigint(10) NULL,
  `uid` varchar(255) NULL,
  `token` longtext NULL,
  `refresh_token` text NULL,
  `expires` datetime NULL,
  `scope` longtext NULL,
  `user_info` text NULL,
  `created_at` datetime default now(),
  `updated_at` datetime default now()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores access tokens for system accounts in order to be able' ROW_FORMAT=COMPRESSED;

ALTER TABLE `c_access_token` ADD FOREIGN KEY (`iduser`) 
REFERENCES `mdl_user`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

original columns
// `id`, `timecreated`, `timemodified`, `usermodified`, 
    `issuerid`, `token`, `expires`, `scope`
*/

class BaseAccessToken{
    private $plainResponse;

    public 
    $issuerid,
    $sub,
    $iduser,
    $token,
    $refresh_token,
    $expires,
    $scope,
    $uid, // custom uid;
    $user_info; // userinfo plain json 

    public function __construct($plainResponse = null){
        $this->setPlain($plainResponse);
    }

    public function setPlain($plainResponse = null){
        $this->plainResponse = $plainResponse;
    }
    
    public function encode(
        $issuerid = "",
        $sub = "",
        $iduser = "",
        
        $token = "",
        $refresh_token = "",
        
        $expires = "",
        $scope = "",
        $uid = "",
        $user_info = "{}"
    ){
        $this->issuerid = (int) $issuerid;
        $this->sub = $sub;
        $this->iduser = (int) $iduser;
        $this->token = $token;
        $this->refresh_token = $refresh_token;
        $this->expires = $expires;
        $this->scope = $scope;
        $this->uid = $uid;
        $this->user_info = $user_info;

        return json_encode($this);
    }

    public function decode(){
        $decoded = json_decode($this->plainResponse);

        $this->issuerid = (int) $decoded->issuerid;
        $this->sub = $decoded->sub;
        $this->iduser = (int) $decoded->iduser;
        
        $this->token = $decoded->token;
        $this->refresh_token = $decoded->refresh_token;

        $this->expires = $decoded->expires;
        $this->scope = $decoded->scope;
        $this->uid = $decoded->uid;
        $this->user_info = $decoded->user_info;

        return $this;
    }

    public function toArray(){ 
        return [
            "issuerid" => (int) $this->issuerid,
            "sub" => $this->sub,
            "uid" => $this->uid,
            "iduser" => (int) $this->iduser,
            "token" => $this->token,
            "refresh_token" => $this->refresh_token,
            "expires" => $this->expires,
            "scope" => $this->scope,
            "user_info" => $this->user_info
        ];
    }
}