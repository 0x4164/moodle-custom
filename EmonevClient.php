<?php

include_once("SimpleHTTPClient.php");
include_once('BaseAccessToken.php');

// abstract class AbstractJsonData{
//     abstract function decode();
// }

class EmonevClient{
    const BASE_URL = "https://elearning.kejaksaan.go.id/e-monev/";
    // const BASE_URL = "http://localhost/e-monev/";

    const URL_TOKEN_SUBMIT = "api/moodle/token/submit";
    const URL_REFRESH = "v2/oauth/token";
    
    private function clientPost($url, $body){
        $client = new SimpleHTTPClient();
        $ret = $client->post($url, 
            $body
        );

        return $ret;
    }

    private function jaksaGet($url, $body){
        $client = new SimpleHTTPClient();
        $ret = $client->get($url, 
            $body
        );

        return $ret;
    }

	public function submitToken($baseAccToken){
        $url = static::BASE_URL . static::URL_TOKEN_SUBMIT;
        $body = [
            // BaseAccessToken / str type 
            'baseAccToken' => $baseAccToken
        ];
        
        $response = $this->clientPost($url, $body);

		return $response['body'];
	}

	public function refreshToken($userId){
        $url = static::BASE_URL . statis::URL_REFRESH;
		return "json refreshtoken";
	}
}